package me.koenn.core.pluginmanager;

import me.koenn.core.KoennCore;
import me.koenn.core.command.Command;
import me.koenn.core.data.JSONManager;
import me.koenn.core.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class ReloadConfigCommand extends Command {

    public ReloadConfigCommand() {
        super("reloadconfig", "/reloadconfig <plugin>");
    }

    @Override
    public boolean execute(CPlayer player, String[] args) {
        if (!player.getPlayer().isOp()) {
            return false;
        }
        if (args.length < 1) {
            return false;
        }

        Plugin plugin = Bukkit.getPluginManager().getPlugin(args[0]);
        if (plugin == null) {
            return false;
        }
        PluginManager pluginManager = PluginManager.getPluginManager(plugin);
        if (pluginManager == null) {
            return false;
        }

        KoennCore.log("Reloading all files for plugin \'" + plugin.getName() + "\'...");
        int json = 0;
        for (JSONManager jsonManager : pluginManager.getJsonManagers()) {
            jsonManager.reload();
            json++;
        }
        KoennCore.log("Reloaded " + json + " json files!");
        plugin.reloadConfig();
        KoennCore.log("Reloaded config file!");
        player.sendMessage(ChatColor.GREEN + "Successfully reloaded all files for plugin '" + plugin.getName() + "'");
        return true;
    }
}
