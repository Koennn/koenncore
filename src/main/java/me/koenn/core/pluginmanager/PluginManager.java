package me.koenn.core.pluginmanager;

import me.koenn.core.KoennCore;
import me.koenn.core.data.JSONManager;
import me.koenn.core.registry.Registry;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class PluginManager {

    public static final Registry<PluginManager> pluginRegistry = new Registry<>(pluginManager -> pluginManager.getPlugin().getName());

    private final Plugin plugin;
    private final List<JSONManager> jsonManagers;

    public PluginManager(Plugin plugin) {
        this.plugin = plugin;
        this.jsonManagers = new ArrayList<>();
        KoennCore.log("Registered PluginManager for plugin \'" + plugin.getName() + "\'");
    }

    public static void registerPlugin(Plugin plugin) {
        pluginRegistry.register(new PluginManager(plugin));
    }

    public static PluginManager getPluginManager(Plugin plugin) {
        return pluginRegistry.get(plugin.getName());
    }

    public static void registerJSONManager(JSONManager jsonManager, Plugin plugin) {
        pluginRegistry.get(plugin.getName()).jsonManagers.add(jsonManager);
    }

    public List<JSONManager> getJsonManagers() {
        return jsonManagers;
    }

    public Plugin getPlugin() {
        return plugin;
    }
}
