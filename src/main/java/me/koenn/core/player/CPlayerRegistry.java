package me.koenn.core.player;

import me.koenn.core.KoennCore;
import me.koenn.core.data.JSONManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("unused")
public final class CPlayerRegistry {

    private final ArrayList<CPlayer> cPlayers;

    public CPlayerRegistry() {
        KoennCore.log("Loading CPlayerRegistry...");
        this.cPlayers = new ArrayList<>();
    }

    public static void loadCPlayers() {
        int playerCount = 0;
        JSONArray playerList;

        try {
            playerList = (JSONArray) KoennCore.getJsonManager().getBody().get("players");
        } catch (NullPointerException ex) {
            return;
        }
        if (playerList == null || playerList.isEmpty()) {
            return;
        }

        for (Object object : playerList) {
            CPlayerRegistry.registerCPlayer(new CPlayer((JSONObject) object));
            playerCount++;
        }

        try {
            KoennCore.getCPlayerRegistry().savePlayers();
        } catch (IOException e) {
            KoennCore.log("Failed to save CPlayers!");
        }
        KoennCore.log("Loaded " + playerCount + " CPlayers.");
    }

    public static void registerCPlayer(CPlayer cPlayer) {
        for (CPlayer player : KoennCore.getCPlayerRegistry().cPlayers) {
            if (player.getUUID().equals(cPlayer.getUUID())) {
                return;
            }
        }
        KoennCore.getCPlayerRegistry().cPlayers.add(cPlayer);
    }

    public static CPlayer getCPlayer(UUID uuid) {
        for (CPlayer player : KoennCore.getCPlayerRegistry().cPlayers) {
            if (player.getUUID().equals(uuid)) {
                return player;
            }
        }
        return null;
    }

    public static CPlayer getCPlayer(String name) {
        for (CPlayer player : KoennCore.getCPlayerRegistry().cPlayers) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    public static List<CPlayer> getCPlayers() {
        return KoennCore.getCPlayerRegistry().cPlayers;
    }

    public void savePlayers() throws IOException {
        JSONManager manager = KoennCore.getJsonManager();
        //JSONArray playerList = this.cPlayers.stream().map(cPlayer -> new JSONObject(cPlayer.getPlayerData())).collect(Collectors.toCollection(JSONArray::new));
        JSONArray playerList = new JSONArray();
        for (CPlayer cPlayer : cPlayers) {
            JSONObject object = new JSONObject();
            for (String key : cPlayer.getPlayerData().keySet()) {
                object.put(key, cPlayer.getPlayerData().get(key));
            }
            playerList.add(object);
        }
        manager.getBody().put("players", playerList);
        manager.saveBodyToFile();
    }
}
