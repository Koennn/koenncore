package me.koenn.core.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

@SuppressWarnings("unused")
public class DragDropEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final Player player;
    private final ItemStack cursor;
    private final ItemStack item;
    private boolean handled;

    public DragDropEvent(Player player, ItemStack cursor, ItemStack item) {
        this.player = player;
        this.cursor = cursor;
        this.item = item;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

    public Player getPlayer() {
        return player;
    }

    public ItemStack getCursor() {
        return cursor;
    }

    public ItemStack getItem() {
        return item;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
