package me.koenn.core.registry;

public interface RegistryKey<T> {

    String getKey(T object);
}
