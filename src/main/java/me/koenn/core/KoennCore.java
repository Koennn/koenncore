package me.koenn.core;

import me.koenn.core.cgive.CGiveCommandHandler;
import me.koenn.core.command.CommandAPI;
import me.koenn.core.config.ConfigManager;
import me.koenn.core.data.JSONManager;
import me.koenn.core.listeners.ChatListener;
import me.koenn.core.listeners.InventoryClickListener;
import me.koenn.core.listeners.InventoryCloseListener;
import me.koenn.core.listeners.PlayerJoinListener;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.core.pluginmanager.PluginManager;
import me.koenn.core.pluginmanager.ReloadConfigCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * KoennCore is a core instance containing lot's of useful and important features to create plugins.
 * <p>
 * This is the KoennCore main class, containing some static fields of singleton objects.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
public final class KoennCore extends JavaPlugin {

    private static Plugin instance;
    private static CPlayerRegistry cPlayerRegistry;
    private static JSONManager jsonManager;
    private static ConfigManager configManager;
    private static CommandAPI commandAPI;

    public static Plugin getInstance() {
        return instance;
    }

    public static CPlayerRegistry getCPlayerRegistry() {
        return cPlayerRegistry;
    }

    public static JSONManager getJsonManager() {
        return jsonManager;
    }

    public static ConfigManager getConfigManager() {
        return configManager;
    }

    public static CommandAPI getCommandAPI() {
        return commandAPI;
    }

    public static void log(String msg) {
        instance.getLogger().info(msg);
    }

    public static void requireVersion(double version, Plugin plugin) {
        if (Double.parseDouble(instance.getDescription().getVersion()) < version) {
            throw new RuntimeException("Plugin \'" + plugin.getName() + "\' requires KoennCore version " + version + "!");
        }
    }

    @Override
    public void onEnable() {
        instance = this;

        log("All credits for this instance go to Koenn");

        PluginManager.registerPlugin(this);

        cPlayerRegistry = new CPlayerRegistry();
        commandAPI = new CommandAPI();

        configManager = new ConfigManager(this);
        jsonManager = new JSONManager(this, "players.json", false);

        CGiveCommandHandler commandHandler = new CGiveCommandHandler();
        this.getCommand("cgive").setExecutor(commandHandler);
        this.getCommand("cgive").setTabCompleter(commandHandler);

        Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryCloseListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        //Bukkit.getPluginManager().registerEvents(commandAPI, this);

        CommandAPI.registerCommand(new ReloadConfigCommand(), this);

        CPlayerRegistry.loadCPlayers();
    }

    @Override
    public void onDisable() {
        log("All credits for this instance go to Koenn");
    }
}
