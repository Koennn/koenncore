package me.koenn.core.gui;

/**
 * Interface allowing you to listen for when an <code>Option</code> in a <code>Gui</code> is clicked.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential Written by Koen Willemse, May 2017
 */
public interface ClickListener {

    /**
     * Will be called when the <code>Option</code> has been clicked.
     *
     * @param clickedSlot Slot in which the option was clicked
     */
    void click(int clickedSlot);
}