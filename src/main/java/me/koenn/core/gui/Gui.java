package me.koenn.core.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

/**
 * Simple class to create custom gui's.
 * Create an instance of this class, register it using <code>Gui.register</code>
 * and call the <code>open</code> method to open the gui.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public class Gui {

    private static final ArrayList<Gui> guis = new ArrayList<>();
    private final ArrayList<Option> options = new ArrayList<>();
    private final Inventory gui;
    private final OfflinePlayer player;
    private boolean isChest;

    /**
     * Gui simple constructor.
     * Gui size is 9 slots (1 row).
     *
     * @param player  Player for who to show the gui
     * @param guiName Name of the gui
     */
    protected Gui(Player player, String guiName) {
        this(player, guiName, 9);
    }

    /**
     * Gui default constructor.
     *
     * @param player  Player for who to show the gui
     * @param guiName Name of the gui
     * @param size    Size of the gui (amount of inventory slots)
     */
    protected Gui(Player player, String guiName, int size) {
        this.player = player;
        this.gui = Bukkit.createInventory(null, size, guiName);
        this.isChest = false;
    }

    /**
     * Register a <code>Gui</code> class instance.
     * Needs to be called before opening the gui in order for it to work properly.
     *
     * @param gui    Gui class instance to register
     * @param plugin Plugin instance to register the gui to
     */
    public static void registerGui(Gui gui, Plugin plugin) {
        guis.add(gui);
    }

    /**
     * Un-register a <code>Gui</code> class instance.
     * Will automaticly be called when the player closes the gui.
     *
     * @param gui Gui class instance to un-register
     */
    public static void unRegisterGui(Gui gui) {
        guis.remove(gui);
    }

    /**
     * Get the <code>Gui</code> class instance of the gui the player currently has open.
     *
     * @param player Player ot get the gui for
     * @return Gui class instance
     */
    public static Gui getOpenGui(Player player) {
        for (Gui gui : guis) {
            if (!gui.getPlayer().isOnline()) {
                guis.remove(gui);
                continue;
            }
            if (gui.getPlayer().getUniqueId().equals(player.getUniqueId())) {
                return gui;
            }
        }
        return null;
    }

    /**
     * Calculate the amount of rows required for the specified amount of items.
     *
     * @param itemAmount Amount of items to get the required rows for
     * @return Amount of rows required for the specified amount of items
     */
    public static int getRequiredRows(int itemAmount) {
        if (itemAmount == 0) {
            return 9;
        }
        int slots = itemAmount;
        while (slots % 9 != 0) {
            slots++;
        }
        return slots;
    }

    /**
     * Add an <code>Option</code> (button) to the gui.
     *
     * @param option Option to add
     */
    protected void addOption(Option option) {
        this.options.add(option);
        this.gui.addItem(option.getIcon());
    }

    /**
     * Set an <code>Option</code> (button) to a specific slot in the gui.
     *
     * @param slot   Slot to set the option in
     * @param option Option to set in the slot
     */
    protected void setOption(int slot, Option option) {
        this.options.add(option);
        this.gui.setItem(slot, option.getIcon());
    }

    /**
     * Click event function.
     * Will automaticly be called by the event <code>Listener</code>
     *
     * @param event InventoryClickEvent instance
     * @return true if the event needs to be cancelled
     */
    public boolean click(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) {
            return false;
        }
        ItemStack item = event.getCurrentItem();
        if (item.getType().equals(Material.AIR)) {
            return false;
        }
        for (Option option : this.options) {
            if (option.getIcon().getType().equals(item.getType()) && option.getIcon().getAmount() == item.getAmount() && option.getIcon().getDurability() == item.getDurability()) {
                if (option.getIcon().getItemMeta().hasDisplayName()) {
                    if (item.getItemMeta().hasDisplayName()) {
                        if (!option.getIcon().getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())) {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    if (item.getItemMeta().hasDisplayName()) {
                        continue;
                    }
                }
                if (option != Option.placeHolderOption) {
                    return option.run(event.getSlot());
                }
            }
        }
        return false;
    }

    /**
     * Open the gui for the player specified in the constructor.
     */
    public void open() {
        if (this.player.isOnline()) {
            this.player.getPlayer().openInventory(this.gui);
        }
    }

    /**
     * Get the Bukkit <code>Inventory</code> object.
     *
     * @return Bukkit <code>Inventory</code> object
     */
    public Inventory getInventory() {
        return gui;
    }

    /**
     * Get the player which is set in the constructor.
     *
     * @return OfflinePlayer instance
     */
    public OfflinePlayer getPlayer() {
        return player;
    }

    /**
     * Check if the gui functions as a chest (items can be taken out and put in freely).
     *
     * @return true if the gui functions as a chest
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isChest() {
        return isChest;
    }

    /**
     * Set whether the gui needs to function as a chest (items can be taken out and put in freely).
     *
     * @param chest true if the gui needs to function as a chest
     */
    public void setChest(boolean chest) {
        isChest = chest;
    }
}

