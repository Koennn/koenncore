package me.koenn.core.bungee;

import com.google.common.io.ByteArrayDataInput;
import org.bukkit.entity.Player;

public interface PluginMessageReader {

    void read(ByteArrayDataInput input, String channel, Player player, byte[] message);

    String messageType();
}
