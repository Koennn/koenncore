package me.koenn.core.bungee;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class PluginMessageReceiver implements PluginMessageListener {

    private static final List<PluginMessageReader> readers = new ArrayList<>();

    public static void registerReader(PluginMessageReader reader) {
        readers.add(reader);
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }

        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String type = in.readUTF();

        readers.stream().filter(reader -> reader.messageType().equals(type)).forEach(reader -> {
            reader.read(in, channel, player, message);
            readers.remove(reader);
        });
    }
}
