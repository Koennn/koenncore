package me.koenn.core.command;

import me.koenn.core.KoennCore;
import me.koenn.core.misc.ColorHelper;
import me.koenn.core.misc.LoreHelper;
import me.koenn.core.player.CPlayerRegistry;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * API class used to register commands.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public final class CommandAPI implements CommandExecutor, TabCompleter, Listener {

    private final ArrayList<Command> commands = new ArrayList<>();

    /**
     * Do NOT call this constructor.
     * It will only be called by <code>KoennCore</code> itself to register
     * <code>Listener</code>'s and <code>CommandExecutor</code>'s.
     */
    public CommandAPI() {
    }

    /**
     * Register a <code>Command</code> class instance.
     * Command needs to be registered in owning plugin's <code>plugin.yml</code>.
     *
     * @param command <code>Command</code> to register
     * @param plugin  Plugin instance to register the command to
     */
    public static void registerCommand(Command command, JavaPlugin plugin) {
        try {
            plugin.getCommand(command.getName()).setExecutor(KoennCore.getCommandAPI());
        } catch (NullPointerException ex) {
            KoennCore.log("Plugin '" + plugin.getName() + "' tried to register Command '/" + command.getName() + "' but does not own it!");
            return;
        }
        KoennCore.getCommandAPI().commands.add(command);
        KoennCore.log("Plugin '" + plugin.getName() + "' registered Command '/" + command.getName() + "'.");
    }

    /**
     * Register a <code>Command</code> class instance as a subcommand of a specified <code>Command</code> class
     * instance.
     *
     * @param mainCommand Main <code>Command</code> to register your subcommand under
     * @param subCommand  Sub <code>Command</code> class instance to register
     * @param plugin      Plugin instance to register the command to
     */
    public static void registerSubCommand(Command mainCommand, Command subCommand, JavaPlugin plugin) {
        if (!KoennCore.getCommandAPI().commands.contains(mainCommand)) {
            KoennCore.log("Plugin '" + plugin.getName() + "' tried to use mainCommand '/" + mainCommand.getName() + "' but did not register it!");
            return;
        }
        plugin.getCommand(mainCommand.getName()).setTabCompleter(KoennCore.getCommandAPI());
        mainCommand.addSubCommand(subCommand);
        KoennCore.log("Plugin '" + plugin.getName() + "' registered subCommand '/" + mainCommand.getName() + " " + subCommand.getName() + "'.");
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String s, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;

        for (Command command : this.commands) {
            if (command.getName().equalsIgnoreCase(cmd.getName())) {

                if (args.length > 0 && command.hasSubCommands()) {
                    for (Command subCommand : command.getSubCommands()) {
                        if (subCommand.getName().equalsIgnoreCase(args[0])) {
                            if (!subCommand.execute(CPlayerRegistry.getCPlayer(player.getUniqueId()), args)) {
                                player.sendMessage(ColorHelper.readColor(subCommand.getUsage()));
                            }
                            return true;
                        }
                    }
                }

                try {
                    if (!command.execute(CPlayerRegistry.getCPlayer(player.getUniqueId()), args)) {
                        player.sendMessage(ColorHelper.readColor(command.getUsage()));
                    }
                } catch (Exception ex) {
                    KoennCore.log("An error occurred while executing command \'" + command.getName() + "\': " + ex);
                    ex.printStackTrace();
                }
                return true;
            }
        }

        throw new IllegalArgumentException("Unknown command '" + cmd.getName() + "'.");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command cmd, String s, String[] args) {
        for (Command command : this.commands) {
            if (command.getName().equalsIgnoreCase(cmd.getName()) && command.hasSubCommands()) {
                if (args.length == 1) {
                    for (Command subCommand : command.getSubCommands()) {
                        if (subCommand.getName().equals(args[0])) {
                            if (!subCommand.getTabCompleteOptions().isEmpty()) {
                                return subCommand.getTabCompleteOptions();
                            } else {
                                return command.getSubCommands().stream().map(Command::getName).collect(Collectors.toList());
                            }
                        } else if (subCommand.getName().startsWith(args[0])) {
                            return LoreHelper.makeLore(subCommand.getName());
                        }
                    }
                }
                if (args.length > 1) {
                    return null;
                }
                return command.getSubCommands().stream().map(Command::getName).collect(Collectors.toList());
            }
        }
        return null;
    }
}
