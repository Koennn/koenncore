package me.koenn.core.command;

import me.koenn.core.player.CPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract command class.
 * Simply create a class extending this one and register it to the <code>CommandAPI</code> for the command to work.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings({"unused", "BooleanMethodIsAlwaysInverted"})
public abstract class Command {

    private final String name;
    private final String usage;
    private final List<Command> subCommands;
    private final List<String> tabCompleteOptions;

    /**
     * Command default constructor.
     *
     * @param name  Name of the command (eg. example)
     * @param usage Usage of the command (eg. /example)
     */
    public Command(String name, String usage) {
        this.name = name;
        this.usage = usage;
        this.subCommands = new ArrayList<>();
        this.tabCompleteOptions = new ArrayList<>();
    }

    /**
     * Method which will be called when a <code>Player</code> executes the command.
     *
     * @param player Player who called the command
     * @param args   Player specified arguments
     * @return true if the command was handled, false if an error occurred
     */
    public abstract boolean execute(CPlayer player, String[] args);

    /**
     * Get the name of the command.
     *
     * @return Name of the command
     */
    public String getName() {
        return name;
    }

    /**
     * Get the usage of the command
     *
     * @return Usage of the command
     */
    public String getUsage() {
        return usage;
    }

    /**
     * Add a subcommand underneath this command.
     *
     * @param command Subcommand to add
     */
    public void addSubCommand(Command command) {
        this.subCommands.add(command);
    }

    /**
     * Check if the command has any subcommands.
     *
     * @return true if the command has subcommands
     */
    public boolean hasSubCommands() {
        return !this.subCommands.isEmpty();
    }

    /**
     * Get a list with all subcommands of this command.
     *
     * @return List with all subcommands
     */
    public List<Command> getSubCommands() {
        return subCommands;
    }

    /**
     * Get a list with all tab-complete options of this command.
     *
     * @return List with all tab-complete options
     */
    public List<String> getTabCompleteOptions() {
        return tabCompleteOptions;
    }

    /**
     * Add a tab-complete option for this command.
     *
     * @param string tab-complete option to add
     */
    public void addTabCompleteOption(String string) {
        this.tabCompleteOptions.add(string);
    }
}
