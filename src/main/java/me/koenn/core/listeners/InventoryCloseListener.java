package me.koenn.core.listeners;

import me.koenn.core.gui.Gui;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public final class InventoryCloseListener implements Listener {

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        Gui gui = Gui.getOpenGui((Player) event.getPlayer());
        if (gui != null) {
            Gui.unRegisterGui(gui);
        }
    }
}
