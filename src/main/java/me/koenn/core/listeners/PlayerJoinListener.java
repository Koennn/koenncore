package me.koenn.core.listeners;

import me.koenn.core.KoennCore;
import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.IOException;

public final class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        CPlayerRegistry.registerCPlayer(new CPlayer(event.getPlayer().getUniqueId()));
        try {
            KoennCore.getCPlayerRegistry().savePlayers();
        } catch (IOException e) {
            KoennCore.log("Failed to register player '" + event.getPlayer().getName() + "'!");
        }
    }
}
