package me.koenn.core.listeners;

import me.koenn.core.KoennCore;
import me.koenn.core.events.DragDropEvent;
import me.koenn.core.gui.Gui;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public final class InventoryClickListener implements Listener {

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onInventoryClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getWhoClicked();

        Gui gui = Gui.getOpenGui(player);
        if (gui != null) {
            try {
                if (!(event.getClickedInventory() instanceof PlayerInventory)) {
                    if (!gui.isChest() && gui.click(event)) {
                        event.setCancelled(true);
                    } else {
                        gui.click(event);
                    }
                } else if (event.getClick().name().contains("SHIFT")) {
                    if (!gui.isChest()) {
                        event.setCancelled(true);
                    }
                }
            } catch (Exception ex) {
                KoennCore.log("An error occurred in \'" + gui.getClass().getSimpleName() + "\': " + ex);
                ex.printStackTrace();
            }
        }

        if (event.getCursor() == null || event.getCurrentItem() == null) {
            return;
        }
        if (event.getCursor().getType().equals(Material.AIR) || event.getCurrentItem().getType().equals(Material.AIR)) {
            return;
        }

        DragDropEvent dragDropEvent = new DragDropEvent(player, event.getCursor(), event.getCurrentItem());
        Bukkit.getPluginManager().callEvent(dragDropEvent);
        if (dragDropEvent.isHandled()) {
            event.setCancelled(true);
            event.setCursor(new ItemStack(Material.AIR));
        }
    }
}
