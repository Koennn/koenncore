package me.koenn.core.cgive;

import me.koenn.core.KoennCore;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;

@SuppressWarnings("unused")
public final class CGiveAPI {

    private static final ArrayList<CItem> cItems = new ArrayList<>();

    public static void registerCItem(CItem cItem, Plugin plugin) {
        cItems.add(cItem);
        KoennCore.log("Plugin '" + plugin.getName() + "' registered CItem '" + cItem.getName() + "'.");
    }

    public static CItem getCItem(String name) {
        for (CItem cItem : cItems) {
            if (cItem.getName().equalsIgnoreCase(name)) {
                return cItem;
            }
        }
        return null;
    }

    public static ArrayList<CItem> getCItems() {
        return cItems;
    }
}
