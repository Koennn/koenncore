package me.koenn.core.cgive;

import me.koenn.core.misc.LoreHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.stream.Collectors;

public final class CGiveCommandHandler implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            return false;
        }

        if (!sender.hasPermission("cgive.give")) {
            return false;
        }

        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;

        CItem cItem = CGiveAPI.getCItem(args[0]);
        if (cItem == null) {
            return false;
        }

        int amount;
        try {
            amount = Integer.parseInt(args[1]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            amount = 1;
        }
        ItemStack item = cItem.getItem();
        item.setAmount(amount);

        player.getInventory().addItem(item);
        return true;
    }


    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        if (args.length > 0) {
            for (CItem cItem : CGiveAPI.getCItems()) {
                if (cItem.getName().startsWith(args[0])) {
                    if (cItem.getName().equals(args[0])) {
                        return CGiveAPI.getCItems().stream().map(CItem::getName).collect(Collectors.toList());
                    } else {
                        return LoreHelper.makeLore(cItem.getName());
                    }
                }
            }
        }
        return CGiveAPI.getCItems().stream().map(CItem::getName).collect(Collectors.toList());
    }
}
