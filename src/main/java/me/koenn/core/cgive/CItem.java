package me.koenn.core.cgive;

import org.bukkit.inventory.ItemStack;

public interface CItem {

    ItemStack getItem();

    String getName();
}
