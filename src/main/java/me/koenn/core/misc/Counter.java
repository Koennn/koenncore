package me.koenn.core.misc;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 * Simple class which allows you to easily create counters.
 * Create an instance of the class and call the <code>start</code> method to start the counter.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public abstract class Counter {

    private final Plugin pl;
    private int timeLeft;
    private int task;
    private boolean paused;

    /**
     * Constructor for the Counter object.
     *
     * @param timeLeft Time left on the counter
     * @param pl       Instance of the plugin
     */
    public Counter(int timeLeft, Plugin pl) {
        this.timeLeft = timeLeft;
        this.pl = pl;
    }

    /**
     * Start the counter and call the callback when the time runs out.
     *
     * @param callback Runnable to run when the time runs out
     */
    public void start(Runnable callback) {
        //Check if the time is set to -1.
        if (this.timeLeft == -1) {
            return;
        }

        //Start a repeating task to run every second.
        this.task = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, () -> {
            //Check if the timer is paused, and return if so.
            if (this.paused) {
                return;
            }

            //Decrease the time left.
            this.timeLeft--;
            this.onCount(this.timeLeft);

            //Run the callback and stop the task when the time runs out.
            if (this.timeLeft == 0) {
                callback.run();
                Bukkit.getScheduler().cancelTask(this.task);
            }
        }, 0, 20);
    }

    public abstract void onCount(int timeLeft);

    /**
     * Pause the counter.
     */
    public void pause() {
        this.paused = true;
    }

    /**
     * Continue the counter if it's paused.
     */
    public void play() {
        this.paused = false;
    }

    public boolean isPaused() {
        return paused;
    }

    /**
     * Completely stop the counter.
     */
    public void stop() {
        this.timeLeft = 0;
    }

    /**
     * Get the time left on the counter.
     *
     * @return int timeLeft
     */
    public int getTimeLeft() {
        return timeLeft;
    }
}

