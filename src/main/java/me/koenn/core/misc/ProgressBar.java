package me.koenn.core.misc;

import org.bukkit.ChatColor;

/**
 * Useful class for creating progress bars.
 * Simply create an instance of this class and it allows you to get the progress string based of a percentage.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, April 2017
 */
@SuppressWarnings("unused")
public class ProgressBar {

    private final int barWidth;
    private final ChatColor fullColor;
    private final ChatColor emptyColor;
    private final char progressChar;

    /**
     * ProgressBar simple constructor.
     * Creates a progress bar with all default properties.
     * Only the barWidth needs to be specified.
     * Default values:
     * - fullColor = GREEN
     * - emptyColor = RED
     * - progressChar = |
     *
     * @param barWidth How many characters wide the bar is
     */
    public ProgressBar(int barWidth) {
        this(barWidth, ChatColor.GREEN, ChatColor.RED, '|');
    }

    /**
     * ProgressBar default constructor.
     * Creates a ProgressBar with the specified properties.
     *
     * @param barWidth     How many characters wide the bar is
     * @param fullColor    What the fill color of the progress bar is
     * @param emptyColor   What the empty color of the progress bar is
     * @param progressChar What character to user to display the progress bar
     */
    public ProgressBar(int barWidth, ChatColor fullColor, ChatColor emptyColor, char progressChar) {
        this.barWidth = barWidth;
        this.fullColor = fullColor;
        this.emptyColor = emptyColor;
        this.progressChar = progressChar;
    }

    /**
     * Get the progress string filled to a certain percentage.
     *
     * @param percentage Percentage to fill the bar with
     * @return Progress string filled to the specified percentage
     */
    public String get(int percentage) {
        StringBuilder builder = new StringBuilder(this.barWidth);
        final int scaledProgress = Math.round(calculateScaledProgress(percentage, this.barWidth));
        final int remainder = this.barWidth - scaledProgress;

        for (int i = 0; i < scaledProgress; i++) {
            builder.append(this.fullColor).append(ChatColor.BOLD).append(this.progressChar);
        }
        for (int i = 0; i < remainder; i++) {
            builder.append(this.emptyColor).append(ChatColor.BOLD).append(this.progressChar);
        }

        return builder.toString();
    }

    private float calculateScaledProgress(float current, float maxSize) {
        return current < (float) 100 ? current > (float) 0 ? current * maxSize / (float) 100 : 0 : maxSize;
    }
}
