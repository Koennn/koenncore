package me.koenn.core.misc;

import org.bukkit.ChatColor;

/**
 * Simple class which allows you to easily translate '&' color codes to Bukkit equivalents.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */

@SuppressWarnings("unused")
public final class ColorHelper {

    private ColorHelper() {
    }

    /**
     * Translate all '&' color codes in a string to the Bukkit equivalents.
     *
     * @param string String to translate
     * @return The translated string
     */
    public static String readColor(String string) {
        if (string == null || !string.contains("&")) {
            return string;
        }
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}
