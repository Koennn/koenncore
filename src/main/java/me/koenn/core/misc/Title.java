package me.koenn.core.misc;

import net.minecraft.server.v1_16_R2.IChatBaseComponent;
import net.minecraft.server.v1_16_R2.PacketPlayOutTitle;
import net.minecraft.server.v1_16_R2.PlayerConnection;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * This class allows you to easily send title messages to players.
 * Simply create an instance of the class and send it to any player you want.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public class Title {

    private final String title;
    private final String subtitle;
    private int stay;
    private int fade;

    /**
     * Title default constructor.
     * Create a title and set the title and subtitle messages.
     *
     * @param title    Title message
     * @param subtitle Subtitle message
     */
    public Title(String title, String subtitle) {
        this.title = ColorHelper.readColor(title);
        this.subtitle = ColorHelper.readColor(subtitle);
        this.stay = 4;
    }

    /**
     * Set how long the title will stay visible after it has been send in seconds.
     * Default is 4 seconds.
     *
     * @param stay The amount of seconds for the title to stay
     * @return Title object for easy construction
     */
    public Title setStay(int stay) {
        this.stay = stay;
        return this;
    }

    /**
     * Set how long the title will fade in and out in seconds.
     * Default is 0 seconds.
     *
     * @param fade The amount of seconds for the title to fade
     * @return Title object for easy construction
     */
    public Title setFade(int fade) {
        this.fade = fade;
        return this;
    }

    /**
     * Send the title message to a certain player
     *
     * @param player Player to send the title message to
     */
    public void send(Player player) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        PacketPlayOutTitle init = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, this.fade * 20, stay * 20, this.fade * 20);
        connection.sendPacket(init);
        if (this.subtitle != null) {
            IChatBaseComponent icbcsubtitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + this.subtitle + "\"}");
            PacketPlayOutTitle subtitlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, icbcsubtitle);
            connection.sendPacket(subtitlePacket);
        }
        if (this.title != null) {
            IChatBaseComponent icbctitle = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + this.title + "\"}");
            PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, icbctitle);
            connection.sendPacket(titlePacket);
        }
    }
}
