package me.koenn.core.misc;

import net.minecraft.server.v1_16_R2.ChatMessageType;
import net.minecraft.server.v1_16_R2.IChatBaseComponent;
import net.minecraft.server.v1_16_R2.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_16_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * This class allows you to easily send actionbar messages to players.
 * Simply create an instance of the class and send it to any player you want.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, April 2017
 */
@SuppressWarnings("unused")
public class ActionBar {

    private static final String JSON_FORMAT = "{\"text\": \"%s\"}";

    private final String message;
    private final Plugin plugin;
    private int stay = 0;

    /**
     * ActionBar default constructor.
     * Creates a new ActionBar object registered to a certain plugin.
     *
     * @param message Message you want to display in the actionbar
     * @param plugin  Plugin instance to register it to
     */
    public ActionBar(String message, Plugin plugin) {
        this.message = ColorHelper.readColor(message);
        this.plugin = plugin;
        this.stay = 1;
    }

    /**
     * Set how long the actionbar will stay visible after it has been send in seconds.
     * Default is 1 second.
     *
     * @param stay The amount of seconds for the actionbar to stay
     * @return ActionBar object for easy construction
     */
    public ActionBar setStay(int stay) {
        this.stay = stay;
        return this;
    }

    /**
     * Send the actionbar message to a certain player.
     *
     * @param player Player to send the actionbar message to
     */
    public void send(Player player) {
        new BukkitRunnable() {
            int time = 0;

            @Override
            public void run() {
                if (time >= stay) {
                    cancel();
                    return;
                }
                IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a(String.format(JSON_FORMAT, message));
                PacketPlayOutChat bar = new PacketPlayOutChat(icbc, ChatMessageType.GAME_INFO, player.getUniqueId());
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(bar);
                time++;
            }
        }.runTaskTimer(this.plugin, 0L, 20L);
    }
}
