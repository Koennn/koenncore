package me.koenn.core.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility class containing all kinds of useful lore related methods.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public final class LoreHelper {

    private LoreHelper() {
    }

    /**
     * Create a lore List (item tooltip) from an array of strings.
     *
     * @param lore Array of strings to turn into a lore list
     * @return List containing all the lore
     */
    public static List<String> makeLore(String... lore) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list, lore);
        return list;
    }

    /**
     * Create a lore List (item tooltip) from an array of objects.
     * All objects will be translated to strings using the <code>FancyString</code> class.
     *
     * @param lore Array of objects to turn into a lore list
     * @return List containing all the lore
     */
    public static List<String> makeLore(Object... lore) {
        List<String> list = new ArrayList<>();
        for (Object object : lore) {
            list.add(new FancyString(object).toString());
        }
        return list;
    }
}
