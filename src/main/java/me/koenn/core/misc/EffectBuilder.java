package me.koenn.core.misc;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;
import me.koenn.core.KoennCore;
import org.bukkit.plugin.Plugin;

/**
 * This class allows you to easily create particle effects using the EffectLib library. Simply create an instance and
 * set the properties you want the effect to have and call the <code>build</code> function to create the effect.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public class EffectBuilder {

    private final Effect effect;

    /**
     * EffectBuilder default constructor.
     *
     * @param effectClass EffectLib <code>Effect</code> class (determines particle shape).
     * @param plugin      Plugin instance to register the effect to.
     */
    public EffectBuilder(Class<? extends Effect> effectClass, Plugin plugin) {
        Object[] args = new Object[]{new EffectManager(KoennCore.getInstance())};
        this.effect = (Effect) ReflectionHelper.newInstance(effectClass, args);
        if (this.effect == null) {
            KoennCore.getInstance().getLogger().info("Failed create effect '" + effectClass.getName() + "' for plugin '" + plugin.getName() + "'");
        }
    }

    public EffectBuilder iterations(int iterations) {
        this.effect.iterations = iterations;
        return this;
    }

    public EffectBuilder speed(float speed) {
        this.effect.speed = speed;
        return this;
    }

    public EffectBuilder particleEffect(ParticleEffect particleEffect) {
        ReflectionHelper.setField(this.effect, "particle", particleEffect);
        return this;
    }

    public EffectBuilder origin(DynamicLocation origin) {
        this.effect.setDynamicOrigin(origin);
        return this;
    }

    public EffectBuilder target(DynamicLocation target) {
        this.effect.setDynamicTarget(target);
        return this;
    }

    public EffectBuilder callback(Runnable callback) {
        this.effect.callback = callback;
        return this;
    }

    public EffectBuilder property(String fieldName, Object value) {
        ReflectionHelper.setField(this.effect, fieldName, value);
        return this;
    }

    public EffectBuilder location(DynamicLocation location) {
        return this.origin(location).target(location);
    }

    public Effect build() {
        return this.effect;
    }
}
