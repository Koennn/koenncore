package me.koenn.core.misc;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

/**
 * Simple class which allows you to easily create timers and delays.
 * Create an instance of the class and call the <code>start</code> method to start the timer.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public class Timer {

    private final int time;
    private final Plugin plugin;
    private boolean repeat;
    private int currentTask;

    /**
     * No-delay Timer constructor.
     * Allows you to create a timer that waits for a specified amount of ticks.
     * Timer will only run once, and has a delay of 0 ticks.
     *
     * @param plugin Plugin instance to register the timer to
     */
    public Timer(Plugin plugin) {
        this(0, false, plugin);
    }

    /**
     * Timer simple constructor.
     * Allows you to create a timer that waits for a specified amount of ticks.
     * Timer will only run once.
     *
     * @param ticks  Amount of ticks the timer needs to wait for.
     * @param plugin Plugin instance to register the timer to
     */
    public Timer(int ticks, Plugin plugin) {
        this(ticks, false, plugin);
    }

    /**
     * Timer default constructor.
     * Allows you to create a timer that waits for a specified amount of ticks.
     *
     * @param ticks  Amount of ticks the timer needs to wait for.
     * @param repeat Set to true if the timer needs to restart after it finishes
     * @param plugin Plugin instance to register the timer to
     */
    public Timer(int ticks, boolean repeat, Plugin plugin) {
        this.time = ticks;
        this.repeat = repeat;
        this.plugin = plugin;
    }

    /**
     * Start the timer with a specified callback.
     * The callback will be ran as soon as the timer finishes.
     * Will run recursively when <code>repeat</code> is set to true.
     *
     * @param callBack Callback to run when the timer finishes
     */
    public void start(Runnable callBack) {
        this.currentTask = Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> {
            callBack.run();
            if (this.repeat) {
                this.start(callBack);
            }
        }, this.time);
    }

    /**
     * Stop the timer (will only work if repeat is true).
     * Will directly cancel the current execution, and stop the timer.
     */
    public void stop() {
        this.repeat = false;
        Bukkit.getScheduler().cancelTask(this.currentTask);
    }
}
