package me.koenn.core.misc;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Utility class containing all kinds of useful location related methods.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public final class LocationHelper {

    private LocationHelper() {
    }

    /**
     * Translate a Location into a string, allowing for easy storing in files etc.
     *
     * @param location Location to translate
     * @param rotation Set to true if you want rotation to translate with the location
     * @return String containing information about the location
     */
    public static String getString(Location location, boolean rotation) {
        if (rotation) {
            return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", " + location.getYaw() + ", " + location.getPitch();
        } else {
            return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ();
        }
    }

    /**
     * Translate a Location into a string, allowing for easy storing in files etc.
     * Does not translate information about the rotation.
     *
     * @param location Location to translate
     * @return String containing information about the location.
     */
    public static String getString(Location location) {
        return getString(location, false);
    }

    /**
     * Translate a string created with <code>getString</code> back to a Location.
     *
     * @param location Location string to translate back
     * @param rotation Set to true if you want rotation to translate with the location
     * @return Location translated from the string
     */
    public static Location fromString(String location, boolean rotation) {
        String[] arr = location.split(", ");
        World world = Bukkit.getWorld(arr[0]);
        double x = Double.parseDouble(arr[1]);
        double y = Double.parseDouble(arr[2]);
        double z = Double.parseDouble(arr[3]);
        if (rotation) {
            float yaw = Float.parseFloat(arr[4]);
            float pitch = Float.parseFloat(arr[5]);
            return new Location(world, x, y, z, yaw, pitch);
        } else {
            return new Location(world, x, y, z);
        }
    }

    /**
     * Translate a string created with <code>getString</code> back to a Location.
     * Does not translate information about the rotation.
     *
     * @param location Location string to translate back
     * @return Location translated from the string
     */
    public static Location fromString(String location) {
        return fromString(location, false);
    }

    /**
     * Calculate the exact center point between 2 locations.
     */
    public static Location calculateCenter(Location location1, Location location2) {
        return location1.add(location1.subtract(location2).multiply(0.5));
    }
}
