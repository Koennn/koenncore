package me.koenn.core.misc;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class containing all kinds of useful item related methods.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public final class ItemHelper {

    private ItemHelper() {
    }

    /**
     * Check if 2 ItemStacks are similar to eachother.
     */
    public static boolean areSimilar(ItemStack item1, ItemStack item2) {
        return item1.getType().equals(item2.getType()) &&
                item1.getAmount() == item2.getAmount() &&
                item1.getDurability() == item2.getDurability() &&
                areEqual(item1.getEnchantments(), item2.getEnchantments());
    }

    /**
     * Check if 2 EnchantmentMaps are the same.
     */
    public static boolean areEqual(Map<Enchantment, Integer> enchants1, Map<Enchantment, Integer> enchants2) {
        if (enchants1.isEmpty() || enchants2.isEmpty()) {
            return enchants1.isEmpty() && enchants2.isEmpty();
        }
        for (Enchantment enchantment : enchants1.keySet()) {
            if (!enchants2.containsKey(enchantment)) {
                return false;
            } else {
                if (!enchants1.get(enchantment).equals(enchants2.get(enchantment))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Easily create an ItemStack with whatever properties you want.
     *
     * @param type        Type of the item (Bukkit Material)
     * @param amount      Amount of items in the stack
     * @param meta        Metadata of the item (damage value)
     * @param displayName Display name of the item
     * @param lore        Lore of the item (tooltip)
     * @return ItemStack with the specified properties
     */
    public static ItemStack makeItemStack(Material type, int amount, short meta, String displayName, List<String> lore) {
        ItemStack itemStack = new ItemStack(type, amount, meta);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.RESET + displayName);
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    /**
     * Convert an ItemStack to a string for easy storage in files etc.
     *
     * @param itemStack ItemStack to convert to string
     * @return String containing all info about this item
     */
    public static String itemToString(ItemStack itemStack) {
        if (itemStack == null || itemStack.getType() == null || itemStack.getType() == Material.AIR) {
            return null;
        }
        String type = itemStack.getType().toString();
        String amount = String.valueOf(itemStack.getAmount());
        String data = String.valueOf(itemStack.getDurability());
        StringBuilder enchants = new StringBuilder("enchants:");
        if (!itemStack.getEnchantments().isEmpty()) {
            for (Enchantment ench : itemStack.getEnchantments().keySet()) {
                enchants.append(ench.getName()).append("-").append(String.valueOf(itemStack.getEnchantments().get(ench))).append("%");
            }
        } else {
            enchants = new StringBuilder();
        }
        StringBuilder lore = new StringBuilder("lore:");
        if (itemStack.getItemMeta().hasLore()) {
            for (String l : itemStack.getItemMeta().getLore()) {
                lore.append(l.replace(" ", "_")).append("%");
            }
        } else {
            lore = new StringBuilder();
        }
        String name = "name:";
        if (itemStack.getItemMeta().hasDisplayName()) {
            name = name + itemStack.getItemMeta().getDisplayName().replace(" ", "_");
        } else {
            name = "";
        }
        return (type + " " + amount + " " + data + " " + enchants + " " + lore + " " + name).replace("§", "&").trim();
    }

    /**
     * Convert a String (created in the <code>itemToString</code> function back to an ItemStack.
     *
     * @param itemString String to convert back
     * @return ItemStack created from the string
     */
    public static ItemStack stringToItem(String itemString) {
        String[] components = itemString.split(" ");
        Material material = Material.valueOf(components[0].toUpperCase());
        int amount = Integer.parseInt(components[1]);
        short data = Short.parseShort(components[2]);
        ItemStack item = new ItemStack(material, amount, data);
        ItemMeta meta = item.getItemMeta();
        HashMap<Enchantment, Integer> enchantments = getEnchantments(components);
        if (enchantments != null) {
            for (Enchantment e : enchantments.keySet()) {
                meta.addEnchant(e, enchantments.get(e), true);
            }
        }
        List<String> lore = getLore(components);
        if (lore != null) {
            meta.setLore(lore);
        }
        String name = getDisplayName(components);
        if (name != null) {
            meta.setDisplayName(name);
        }
        item.setItemMeta(meta);
        return item;
    }

    private static HashMap<Enchantment, Integer> getEnchantments(String[] components) {
        HashMap<Enchantment, Integer> enchantments = new HashMap<>();
        for (int i = 3; i < components.length; i++) {
            if (components[i].contains("enchants:")) {
                components[i] = components[i].replace("enchants:", "");
                String[] enchants = components[i].split("%");
                for (String e : enchants) {
                    if (e.equalsIgnoreCase("")) {
                        continue;
                    }
                    String[] enchantment = e.split("-");
                    int level = Integer.parseInt(enchantment[1]);
                    enchantments.put(Enchantment.getByName(enchantment[0].toUpperCase()), level);
                }
            }
        }
        if (enchantments.isEmpty()) {
            return null;
        }
        return enchantments;
    }

    private static List<String> getLore(String[] components) {
        List<String> lore = new ArrayList<>();
        for (int i = 3; i < components.length; i++) {
            if (components[i].contains("lore:")) {
                components[i] = components[i].replace("lore:", "");
                String[] lores = components[i].split("%");
                for (String l : lores) {
                    if (l.equalsIgnoreCase("")) {
                        continue;
                    }
                    lore.add(ColorHelper.readColor(l.replace("_", " ")));
                }
            }
        }
        if (lore.isEmpty()) {
            return null;
        }
        return lore;
    }

    private static String getDisplayName(String[] components) {
        String name = null;
        for (int i = 3; i < components.length; i++) {
            if (components[i].contains("name:")) {
                components[i] = components[i].replace("name:", "");
                name = components[i].replace("_", " ");
            }
        }
        return ColorHelper.readColor(name);
    }
}
