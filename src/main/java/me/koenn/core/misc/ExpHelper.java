package me.koenn.core.misc;

import org.bukkit.entity.Player;

/**
 * Easy utility class for all kinds of exp related things.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public final class ExpHelper {

    private ExpHelper() {
    }

    /**
     * Set the total amount of experience of a player.
     *
     * @param player Player to set the experience for
     * @param exp    Amount of experience to set for the player
     */
    public static void setTotalExperience(Player player, int exp) {
        player.setExp(0.0F);
        player.setLevel(0);
        player.setTotalExperience(0);

        int amount = exp;
        while (amount > 0) {
            int expToLevel = getExpAtLevel(player.getLevel());
            amount -= expToLevel;
            if (amount >= 0) {
                player.giveExp(expToLevel);
            } else {
                amount += expToLevel;
                player.giveExp(amount);
                amount = 0;
            }
        }
    }

    /**
     * Get the amount of exp required to level up at a specified level.
     *
     * @param level Level to get the required exp for
     * @return Amount of exp required to level up at this level
     */
    public static int getExpAtLevel(int level) {
        if (level <= 15) {
            return 2 * level + 7;
        }
        if (level <= 30) {
            return 5 * level - 38;
        }
        return 9 * level - 158;
    }

    /**
     * Get the total amount of experience a player has.
     *
     * @param player Player to get the experience from
     * @return Amount of experience the player has
     */
    public static int getTotalExperience(Player player) {
        int exp = Math.round(getExpAtLevel(player.getLevel()) * player.getExp());
        int currentLevel = player.getLevel();
        while (currentLevel > 0) {
            currentLevel--;
            exp += getExpAtLevel(currentLevel);
        }
        if (exp < 0) {
            exp = Integer.MAX_VALUE;
        }
        return exp;
    }
}
