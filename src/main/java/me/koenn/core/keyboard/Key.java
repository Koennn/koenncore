package me.koenn.core.keyboard;

import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;

/**
 * An enumerator containing banner patterns for all letters (A-Z).
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public enum Key {

    A(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE)
    }),

    B(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE)
    }),

    C(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    }),

    D(new Pattern[]{
            new Pattern(DyeColor.BLACK, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    }),

    E(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    }),

    F(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP)
    }),

    G(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP)
    }),

    H(new Pattern[]{
            new Pattern(DyeColor.BLACK, PatternType.BASE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    I(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    J(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    K(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNLEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT)
    }),

    L(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM)
    }),

    M(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLES_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    N(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT)
    }),

    O(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP)
    }),

    P(new Pattern[]{
            new Pattern(DyeColor.BLACK, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.WHITE, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT)
    }),

    Q(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT)
    }),

    R(new Pattern[]{
            new Pattern(DyeColor.BLACK, PatternType.BASE),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT),
            new Pattern(DyeColor.WHITE, PatternType.HALF_VERTICAL),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_MIDDLE)
    }),

    S(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.SQUARE_TOP_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.SQUARE_BOTTOM_LEFT),
            new Pattern(DyeColor.WHITE, PatternType.RHOMBUS_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT)
    }),

    T(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_CENTER)
    }),

    U(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    V(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNLEFT)
    }),

    W(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.WHITE, PatternType.TRIANGLES_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_LEFT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_RIGHT)
    }),

    X(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNLEFT)
    }),

    Y(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNRIGHT),
            new Pattern(DyeColor.WHITE, PatternType.HALF_HORIZONTAL_MIRROR),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNLEFT)
    }),

    Z(new Pattern[]{
            new Pattern(DyeColor.WHITE, PatternType.BASE),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_BOTTOM),
            new Pattern(DyeColor.BLACK, PatternType.TRIANGLE_TOP),
            new Pattern(DyeColor.BLACK, PatternType.SQUARE_BOTTOM_RIGHT),
            new Pattern(DyeColor.BLACK, PatternType.SQUARE_TOP_LEFT),
            new Pattern(DyeColor.WHITE, PatternType.RHOMBUS_MIDDLE),
            new Pattern(DyeColor.BLACK, PatternType.STRIPE_DOWNLEFT)
    });

    private final Pattern[] patterns;

    /**
     * Key default constructor.
     *
     * @param patterns All banner patterns to add (in order)
     */
    Key(Pattern[] patterns) {
        this.patterns = patterns;
    }

    /**
     * Get all banner patterns to add to the banner (in order).
     *
     * @return all banner patterns to add to the banner (in order)
     */
    public Pattern[] getPatterns() {
        return patterns;
    }
}
