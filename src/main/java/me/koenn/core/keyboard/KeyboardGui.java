package me.koenn.core.keyboard;

import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import me.koenn.core.misc.FancyString;
import me.koenn.core.misc.ItemHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Gui containing all keys (A-Z), a space bar and a backspace for user input.
 * Simply create an instance of the class, register it with <code>Gui.registerGui</code>
 * and call <code>gui.open</code> to use the gui.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential Written by Koen Willemse, May 2017
 */
@SuppressWarnings("unused")
public class KeyboardGui extends Gui {

    private final ItemStack result;
    private final boolean fancyFormatting;
    private String input = "";

    /**
     * KeyboardGui simple constructor.
     * Creates a new KeyboardGui with the specified properties.
     * Will automaticly run user input through a <code>FancyString</code>
     * before displaying.
     *
     * @param player   Player for who to show the gui
     * @param subject  The input subject (what do you want the user to type in)
     * @param listener The listener to call when the user presses the confirm button
     */
    public KeyboardGui(Player player, String subject, InputListener listener) {
        this(player, subject, listener, true);
    }

    /**
     * KeyboardGui default constructor.
     * Creates a new KeyboardGui with the specified properties.
     *
     * @param player          Player for who to show the gui
     * @param subject         The input subject (what do you want the user to type in)
     * @param listener        The listener to call when the user presses the confirm button
     * @param fancyFormatting true if you want the user input to run through a <code>FancyString</code> before
     *                        displaying
     */
    public KeyboardGui(Player player, String subject, InputListener listener, boolean fancyFormatting) {
        super(player, "Enter " + subject, 54);
        this.fancyFormatting = fancyFormatting;

        this.result = ItemHelper.makeItemStack(
                Material.PAPER, 1, (short) 0, subject + ": ", null
        );
        this.setResult();

        for (int i = 0; i < 26; i++) {
            final String letter = Character.toString((char) (i + 65));
            this.setOption(i + 18, new Option(KeyboardHelper.getKey(letter), () -> {
                this.input += letter;
                this.displayData(subject);
                this.setResult();
            }));
        }
        this.setOption(44, new Option(ItemHelper.makeItemStack(
                Material.WHITE_BANNER, 1, (short) 15, ChatColor.WHITE + "SPACE", null
        ), () -> {
            this.input += " ";
            this.displayData(subject);
            this.setResult();
        }));

        this.setOption(48, new Option(ItemHelper.makeItemStack(
                Material.WHITE_BANNER, 1, (short) 1, ChatColor.WHITE + "BACKSPACE", null
        ), () -> {
            if (this.input.length() == 0) {
                return;
            }
            this.input = this.input.substring(0, this.input.length() - 1);
            this.displayData(subject);
            this.setResult();
        }));

        this.setOption(50, new Option(ItemHelper.makeItemStack(
                Material.WHITE_BANNER, 1, (short) 10, ChatColor.WHITE + "CONFIRM", null
        ), () -> {
            player.closeInventory();
            listener.run(this.input);
        }));
    }

    private void displayData(String subject) {
        ItemMeta meta = result.getItemMeta();
        meta.setDisplayName(subject + ": " + (this.fancyFormatting ? new FancyString(this.input).toString() : this.input));
        result.setItemMeta(meta);
    }

    private void setResult() {
        this.setOption(4, new Option(this.result, this::setResult));
    }
}
