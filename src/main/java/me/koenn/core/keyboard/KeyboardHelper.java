package me.koenn.core.keyboard;

import me.koenn.core.misc.ItemHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;


/**
 * Utility class containing useful keyboard methods.
 * Do not create an instance of this class, only static methods need to be called.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
public final class KeyboardHelper {

    private KeyboardHelper() {
    }

    /**
     * Get the banner ItemStack for a specified letter.
     *
     * @param letter Letter to get the ItemStack for
     * @return Banner ItemStack with the letter shape
     */
    public static ItemStack getKey(String letter) {
        ItemStack itemStack = ItemHelper.makeItemStack(Material.WHITE_BANNER, 1, (short) 0, ChatColor.WHITE + letter, null);
        BannerMeta meta = (BannerMeta) itemStack.getItemMeta();
        Pattern[] patterns = Key.valueOf(letter.toUpperCase()).getPatterns();

        meta.setBaseColor(patterns[0].getColor());
        for (int i = 1; i < patterns.length; i++) {
            meta.addPattern(patterns[i]);
        }

        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

        itemStack.setItemMeta(meta);
        return itemStack;
    }
}
