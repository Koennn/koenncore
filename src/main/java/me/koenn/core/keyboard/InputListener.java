package me.koenn.core.keyboard;

/**
 * Interface used to listen for a user pressing the confirm button in a
 * <code>KeyboardGui</code> and reading the user input.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, May 2017
 */
public interface InputListener {

    /**
     * The run method will be called whenever the user presses the confirm button.
     *
     * @param input A string with whatever the user typed in
     */
    void run(String input);
}
